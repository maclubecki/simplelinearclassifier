#include "linearclass.h"

void checkLine(const std::vector<double> &v, const std::vector<int> &c, const std::vector<int> &class_list,
               const std::vector<bool> &erased, const double line, std::pair<int,int> &result, int &side, conflict_st &conflict){
    //przeszukiwanie zbioru dla pojedynczej linii
    //RETURNS: CLASS ID (ujemna jesli oddziela punkty 'z lewej')| LICZBA OBIEKTOW
    std::vector< std::pair<int,int> > groups_l, groups_r;
    for(int i = 0; i < class_list.size(); ++i){
        groups_l.push_back(std::pair<int,int>(class_list.at(i), 0));
        groups_r.push_back(std::pair<int,int>(class_list.at(i), 0));
    }
    //zmienne te oznaczaja czy kontynuowac przeszukiwanie na lewo lub prawo
    //checkgroups sprawdza czy oddzielilo tylko 1 klase dalej
    bool go_l = true, go_r = true;

    for(int i = 0; i < v.size(); ++i){
        if(go_r)
            go_r = checkGroups(groups_r);
        if(go_l)
            go_l = checkGroups(groups_l);

        if(!go_l && !go_r){
            conflict = cheapestCut(v,c,erased,groups_l,groups_r,line,i);
            return;
        }

        if(v.at(i) > line && go_r && !erased.at(i)){
            int ind = fIndex(groups_r, c.at(i));
            if(ind != -1) ++groups_r.at(ind).second;
        }
        if(v.at(i) < line && go_l && !erased.at(i)){
            int ind = fIndex(groups_l, c.at(i));
            if(ind != -1) ++groups_l.at(ind).second;
        }
    }

    go_l = checkGroups(groups_l);
    go_r = checkGroups(groups_r);

    //result - porownac max z lewej i prawej i zwrocic wiekszy
    std::pair<int,int> r1(0,0), r2(0,0);
    int p;//bad programming
    if(go_l)
        r1 = maxGroup(groups_l,p);
    if(go_r)
        r2 = maxGroup(groups_r,p);

    if(r1.second >= r2.second && go_l) {
        result = r1;
        side = -1;
    }
    else if(r2.second > r1.second && go_r){
        result = r2;
        side = 1;
    }
    if(result.second == 0)
        conflict = cheapestCut(v,c,erased,groups_l,groups_r,line,v.size());

}

void searchAttribute(const std::vector<double> &v, const std::vector<int> &c, const std::vector<int> &class_list, const std::vector<bool> &erased,
                     const double step, const int col_num, const std::pair<double,double> &minmax, std::pair<int,int> &result, conflict_st &conflict_result,
                     double &result_line, int &result_side){
    //funkcja ta zwraca ID klasy i liczbe osobnikow oddzielonych linia
    //ktora separuje najwiecej osobnikow dla tego wymiaru
    std::vector<std::pair<int,int>> results;
    std::vector<int> results_sides;
    std::vector<conflict_st> conflicts;
    std::vector<double> lines;
    double l = minmax.first + step;
    while( l < minmax.second ){
        conflicts.push_back(conflict_st());
        conflicts.back().attribute = col_num;
        results.push_back(std::pair<int,int>(0,0));
        results_sides.push_back(1);
        lines.push_back(l);
        checkLine(v,c,class_list,erased,l,results.back(),results_sides.back(), conflicts.back());
        l+=step;
    }
    //conflicts check
    int conflict_count = 0;
    for(int i = 0; i < results.size(); ++i){
        if(results.at(i).second == 0 || conflicts.at(i).adjustment > 0)
            ++conflict_count;
    }

    int index;
    //if no good results have been found
    if(conflict_count == lines.size()){
        conflict_result = findBestCut(conflicts, index);
        conflict_result.attribute = col_num;
    }
    //if at least one positive result
    else result = maxGroup(results, index);

    result_line = lines.at(index);
    result_side = results_sides.at(index);

}

bool splitStep(std::vector<std::vector<double>*> &v, const std::vector<int> &c, const std::vector<int> &known_classes,
                                std::vector<bool> &erased, std::vector<std::vector<int>*> &binary_vectors, std::vector<int> &dir_results,
                                std::vector<int> &cut_points, int &cut_count, std::pair<int,double> &final_result){
    //RETURNS <COLUMN ID, LINE>
    /*
    std::vector<std::pair<int,int>> results;
    std::vector<std::pair<double,double>> minmaxes;
    std::vector<conflict_st> results_conflicts;
    std::vector<int> results_sides;
    std::vector<double> steps;
    std::vector<double> results_lines;
    std::vector<std::thread> threads;
    const int count = 100;

    for(int i = 0; i < v.size(); ++i){
        minmaxes.push_back(vectMinMax(*v.at(i),erased));
        steps.push_back(double(minmaxes.back().second - minmaxes.back().first)/count);
        results.push_back(std::pair<int,int>(0,0) );
        results_conflicts.push_back(conflict_st());
        results_lines.push_back(0.0);
        results_sides.push_back(1);
    }

    clock_t begin = clock();
    for (int i = 0; i < v.size(); ++i){
        threads.push_back(std::thread(searchAttribute,*v.at(i), c, known_classes, erased, steps.at(i), minmaxes.at(i),
                          std::ref(results.at(i)), std::ref(results_conflicts.at(i)), std::ref(results_lines.at(i)), std::ref(results_sides.at(i)) ));
    }
    for(int i = 0; i < v.size(); ++i)
        threads.at(i).join();
    */

    std::vector<std::pair<int,int>> results;
    std::vector<int> results_sides;
    std::vector<double> results_lines;
    std::vector<conflict_st> results_conflicts;
    const int count = 100;

    clock_t begin = clock();

    for(int i = 0; i < v.size(); ++i){
        std::pair<double,double> minmax = vectMinMax(*v.at(i), erased);
        const double step = double(minmax.second - minmax.first)/count;
        results.push_back(std::pair<int,int>(0,0) );
        results_conflicts.push_back(conflict_st());
        results_sides.push_back(1);
        results_lines.push_back(0.0);
        //std::cerr << "Col: " << i << "\n";
        searchAttribute(*v.at(i), c, known_classes, erased, step, i, minmax, results.back(),results_conflicts.back(),
                        results_lines.back(), results_sides.back());
    }

    ///////////////////czesc niezalezna od zrownoleglania:

    int index;
    int separated_class;
    int conflict_count = 0;
    for(int i = 0; i < results.size(); ++i){
        if(results.at(i).second == 0 || results_conflicts.at(i).adjustment > 0)
            ++conflict_count;
    }

    //jesli usuwamy punkty
    if(conflict_count == results_conflicts.size()){
        auto cheapest = findBestCut(results_conflicts, index);
        cutPoints(*v.at(cheapest.attribute),c,erased,cheapest.class_id,cheapest.line,cheapest.line_dir,cut_points,cut_count);
        separated_class = cheapest.class_id;
        return false;
    }
    //jesli nie trzeba usuwac punktow
    else{
        std::pair<int,int> best_result = maxGroup(results, index);
        separated_class = best_result.first;
        final_result.first = index;
        final_result.second = results_lines.at(index);
        dir_results.push_back(results_sides.at(index));
        finalize(*v.at(index),c, separated_class,results_sides.at(index), erased, results_lines.at(index), binary_vectors);
    }
    clock_t end = clock();
    double secs = double(end - begin) / CLOCKS_PER_SEC;
    //std::cerr << "Time: " << secs << std::endl;
    return true;

}
void finalize(const std::vector<double> &v, const std::vector<int> &c, const int result_class, const int side,
                 std::vector<bool> &erased, const double line, std::vector<std::vector<int>*> &binary_vectors){
    //usuwanie punktow i tworzenie wektorow binarnych
    for(int i = 0; i < v.size(); ++i){
        if(c.at(i) == result_class)//klasy sie zgadzaja
        {
            if((side > 0 && v.at(i) > line)||(side < 0 && v.at(i) < line)){
                erased.at(i) = true;
                binary_vectors.at(i)->push_back(1);
            }
            else
                binary_vectors.at(i)->push_back(0);

        }
        else {
            if((side > 0 && v.at(i) > line)||(side < 0 && v.at(i) < line))
                binary_vectors.at(i)->push_back(1);
            else
                binary_vectors.at(i)->push_back(0);
        }
    }
}
void cutPoints(const std::vector<double> &v, const std::vector<int> &c, std::vector<bool> &erased, const int cut_class,
               const double line, const int line_dir, std::vector<int> &cut_indexes, int &cut_count){
    for(int i = 0; i < v.size(); ++i){
        if(c.at(i) == cut_class && !erased.at(i)){
            if(line_dir > 0){
                if(v.at(i) > line){
                    erased.at(i) = true;
                    cut_indexes.push_back(i);
                    ++cut_count;
                }
            }
            else{
                if(v.at(i) < line){
                    erased.at(i) = true;
                    cut_indexes.push_back(i);
                    ++cut_count;
                }
            }
        }

    }
}
conflict_st findBestCut(const std::vector<conflict_st> &conflicts, int &index){
    //szukamy minimum adjustment
    index = 0;
    auto result = conflicts.front();
    for(int i = 1; i < conflicts.size(); ++i){
        if(result.adjustment > conflicts.at(i).adjustment && conflicts.at(i).adjustment > 0) {
            result = conflicts.at(i);
            index = i;
        }
    }
    return result;
}

conflict_st cheapestCut(const std::vector<double> &v, const std::vector<int> &c, const std::vector<bool> &erased,
                 std::vector<std::pair<int,int>> &groups_l, std::vector<std::pair<int,int>> &groups_r, const double line, const int last_i){
    //od nowa szukanie
    for(int i = 0; i < groups_l.size(); ++i)
    {
        groups_l.at(i).second = 0;
        groups_r.at(i).second = 0;
    }
    for(int i = 0; i < v.size(); ++i){
        if(v.at(i) > line && !erased.at(i)){
            int ind = fIndex(groups_r, c.at(i));
            if(ind != -1) ++groups_r.at(ind).second;
        }
        if(v.at(i) < line && !erased.at(i)){
            int ind = fIndex(groups_l, c.at(i));
            if(ind != -1) ++groups_l.at(ind).second;
        }
    }
    //teraz wybierz takie ciecie ktore pozwoli przy jak najmniejszym kooszcie wyrzucanych punktow oddzielic jak najwiecej punktow
    conflict_st r;
    r.line = line;

    int i1, i2, s1,s2;
    int a_l = countAdjustment(groups_l, i1,s1);
    int a_r = countAdjustment(groups_r, i2,s2);

    if(a_l <= a_r && a_l > 0){
        r.adjustment = a_l;
        r.line_dir = -1;
        r.class_id = groups_l.at(i1).first;
    }
    else{
        r.adjustment = a_r;
        r.line_dir = 1;
        r.class_id = groups_r.at(i2).first;
    }

    return r;
}
int countAdjustment(const std::vector<std::pair<int,int>> &v, int &min_index, int &sum){
    int min = v.at(0).second;
    min_index = 0;
    sum = 0;
    for(int i = 1; i < v.size(); ++i){
        if((v.at(i).second < min && v.at(i).second != 0) || min == 0){
            sum += min;
            min = v.at(i).second;
            min_index = i;
        }
        else sum += v.at(i).second;
    }
    return min;
}
int classifyPoint(const std::vector<std::pair<int,double>> &steps, const std::vector<int> &steps_dir,
                  const std::vector<std::vector<int>*> &binary_vectors, const std::vector<int> &classes,
                  const std::vector<double> &newpoint){
    std::vector<int> newpoint_binary;

    //ustalenie wektora binarnego nowego punktu
    for(int i = 0; i < steps.size(); ++i){
        if(steps_dir.at(i) < 0){
            if(newpoint.at(abs(steps.at(i).first)) <= steps.at(i).second)
                newpoint_binary.push_back(1);
            else
                newpoint_binary.push_back(0);
        }
        else{
            if(newpoint.at(abs(steps.at(i).first)) >= steps.at(i).second)
                newpoint_binary.push_back(1);
            else
                newpoint_binary.push_back(0);
        }
    }

    for(int x : newpoint_binary)
        std::cerr << x << " ";

    //szukanie po wektorze klasy dla punktu
    for(int i = 0; i < binary_vectors.size(); ++i){
        if(vectEquality(newpoint_binary, *binary_vectors.at(i)))
            return classes.at(i);
    }

    return -1;

}
int findElement(const std::vector<int> &v, int e)
{
    for(int i = 0; i < v.size(); ++i)
        if(e == v.at(i))
            return i;
    return -1;
}
std::pair<double,double> vectMinMax(const std::vector<double> &v, const std::vector<bool> &erased){
    std::pair<double,double> minmax;
    int start;
    for(int i = 0; i < erased.size(); ++i)
    {
        if(!erased.at(i)){
            start = i;
            minmax.first = v.at(start);
            minmax.second = v.at(start);
            break;
        }
    }
    for(int i = start; i < v.size(); ++i) {
        if(v.at(i) > minmax.second) minmax.second = v.at(i);
        if(v.at(i) < minmax.first) minmax.first = v.at(i);
    }
    return minmax;
}
std::pair<int,int> maxGroup(const std::vector< std::pair<int,int> > &v, int &index){
    //zwraca pare z wektora ktora ma najwiekszy .second
    auto m = v.at(0);
    index = 0;
    for(int i = 1; i < v.size(); ++i){
        if(m.second < v.at(i).second ){
            m = v.at(i);
            index = i;
        }
    }

    return m;
}
bool vectEquality(const std::vector<int> &a, const std::vector<int> &b){
    if(a.size() != b.size())
        return false;

    for(int i = 0; i < a.size(); ++i)
        if(a.at(i) != b.at(i))
            return false;

    return true;
}

int fIndex(const std::vector<std::pair<int,int> > &v, int cl){
    for(int i = 0; i < v.size(); ++i)
        if(v.at(i).first == cl) return i;
    return -1;
}

bool checkEnd(const std::vector<int> &classes, const std::vector<bool> &erased){
    int firstclass = -1;
    for(int i = 0; i < erased.size(); ++i){
        if(!erased.at(i)){
            if(firstclass == -1)
                firstclass = classes.at(i);
            if(firstclass != classes.at(i))
                return false;
        }
    }
    return true;
}

bool checkGroups(const std::vector<std::pair<int,int> > &groups){
    //sprawdza czy oddzielilo wiecej niz 1 grupe punktow - jesli tak to zwraca false
    int active = -1;
    for(int i = 0; i < groups.size(); ++i){
        if(groups.at(i).second > 0) {
            if(active != -1)
                return false;
            active = i;
        }
    }
    return true;
}
bool verifyInput(const QString &s, const int cols, std::vector<double> &output){

    QStringList x = s.split(QRegExp("(\\s)|;"));
    if(x.back() == "") x.pop_back();

    if(x.size() == cols){
        for(int i = 0; i < x.size(); ++i)
            output.push_back(x.at(i).toDouble());
        return true;
    }
    return false;
}
