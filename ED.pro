#-------------------------------------------------
#
# Project created by QtCreator 2015-10-19T10:26:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

QMAKE_CXXFLAGS += -std=c++11 -Ofast

TARGET = ED
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    linearclass.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    linearclass.h

FORMS    += mainwindow.ui
