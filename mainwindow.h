#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QSplitter>
#include <QFileDialog>
#include <QDebug>
#include <fstream>
#include "qcustomplot.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void sl_openFile();
    void sl_algorithmStep();
    void sl_algorithmFinish();
    void sl_classify();

private:
    Ui::MainWindow *ui;
    QPushButton *btn_openf, *btn_next, *btn_finish, *btn_classify;
    QLineEdit *edit;
    QCustomPlot *plotw;
    std::vector<std::vector<double>*> data;
    std::vector<std::vector<int>*> binary_vectors;
    std::vector<std::pair<int,double>> step_results;
    std::vector<int> step_results_dir;
    std::vector<int> classes;
    std::vector<int> known_classes;
    std::vector<bool> erased;
    QString current_filename;
    int counter;
    int cut_counter;

    void clearData();
    void generateOutput();
    void parseFile(QFile &input_file);
    void plotCutPoints(const std::vector<double> &xcol, const std::vector<double> &ycol, const std::vector<int> &indexes);
    void plotData(QVector<QVector<double>*> &xcol, QVector<QVector<double>*> &ycol,
                  double x_min, double x_max, double y_min, double y_max);
    void plotLine(int axis, double l);
};

#endif // MAINWINDOW_H
