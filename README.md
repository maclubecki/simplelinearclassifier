This is a simple linear classifier implementation made for the data mining course. Made using Qt and QCustomPlot.

The algorithm divides the space with hyperplanes that separate the most objects of a single class. The separated objects are then ignored and the algorithm continues until all objects have been marked as ignored. When no separation can be made possible (i.e. there's objects of more than 1 class present), objects of the least numerous class in the considered subspace are removed from the set, so the algorithm can continue.

The input data classes are visualized as different colors.

![sc1.jpg](https://bitbucket.org/repo/46LMnA/images/3750645013-sc1.jpg)

![sc2.jpg](https://bitbucket.org/repo/46LMnA/images/45628812-sc2.jpg)

![sc3.jpg](https://bitbucket.org/repo/46LMnA/images/3516069385-sc3.jpg)

![sc4.jpg](https://bitbucket.org/repo/46LMnA/images/2578012343-sc4.jpg)