#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "linearclass.h"
#include <iostream>
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    plotw = new QCustomPlot(this);
    btn_openf = new QPushButton("Otwórz plik", this);
    btn_next = new QPushButton("Kolejny krok", this);
    btn_finish = new QPushButton("Dokończ algorytm", this);
    btn_classify = new QPushButton("Klasyfikuj", this);
    edit = new QLineEdit(this);
    counter = 0;
    cut_counter = 0;

    ui->mainToolBar->addWidget(btn_openf);
    ui->mainToolBar->addWidget(new QSplitter(this));
    ui->mainToolBar->addWidget(new QLabel("Punkt do klasyfikacji: ",this));
    ui->mainToolBar->addWidget(edit);
    ui->mainToolBar->addWidget(btn_classify);
    ui->mainToolBar->addWidget(new QSplitter(this));
    ui->mainToolBar->addWidget(btn_next);
    ui->mainToolBar->addWidget(btn_finish);

    this->setWindowTitle("Eksploracja danych");
    this->setFixedSize(800,600);
    this->setCentralWidget(plotw);
    plotw->setInteraction(QCP::iRangeDrag, true);
    plotw->setInteraction(QCP::iRangeZoom, true);
    connect( btn_openf,    SIGNAL(clicked(bool)), this, SLOT(sl_openFile()) );
    connect( btn_next,     SIGNAL(clicked(bool)), this, SLOT(sl_algorithmStep()) );
    connect( btn_finish,   SIGNAL(clicked(bool)), this, SLOT(sl_algorithmFinish()));
    connect( btn_classify, SIGNAL(clicked(bool)), this, SLOT(sl_classify()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sl_classify(){
    if(data.empty()){
        QMessageBox::critical(this, "Błąd", "Nie wczytano zbioru!");
        return;
    }
    if(!checkEnd(classes,erased)){
        QMessageBox::critical(this, "Błąd", "Algorytm nie przeszedł wszystkich iteracji!");
        return;
    }

    std::vector<double> newpoint;

    if(verifyInput(edit->text(), data.size(), newpoint)){
        int c = classifyPoint(step_results, step_results_dir, binary_vectors, classes, newpoint);
        if(c != -1)
            QMessageBox::information(this, "Info", "Obiekt przypisano do klasy " + QString::number(c));
        else
            QMessageBox::critical(this, "Błąd", "Błąd klasyfikacji?!");
    }
    else QMessageBox::critical(this, "Błąd", "Niepoprawnie wprowadzony punkt!");
}

void MainWindow::sl_algorithmFinish(){
    std::vector<int> cut_indexes;
    while(!checkEnd(classes,erased)){
        std::pair<int,double> result;
        if(splitStep(data, classes, known_classes, erased, binary_vectors, step_results_dir, cut_indexes, cut_counter,result)){
            step_results.push_back(result);
            plotLine(result.first, result.second);
            ++counter;
        }
        else{//jesli usunelo punkt
            plotCutPoints(*data.at(0), *data.at(1), cut_indexes);
        }
    }
    QMessageBox::information(this, "Info", "Klasyfikacja zakończona w " + QString::number(counter) + " krokach, usunięto " + QString::number(cut_counter) + " punktów.");
    generateOutput();
}

void MainWindow::sl_algorithmStep(){
    if(checkEnd(classes,erased))
        return;
    std::vector<int> cut_indexes;
    std::pair<int,double> result;
    bool ok = false;
    while(!ok){
        ok = splitStep(data, classes, known_classes, erased, binary_vectors, step_results_dir, cut_indexes, cut_counter, result);
        if(ok){
            step_results.push_back(result);
            plotLine(result.first, result.second);
            ++counter;
            break;
        }
        else
            plotCutPoints(*data.at(0), *data.at(1),cut_indexes);
    }

    if(checkEnd(classes,erased)){
        QMessageBox::information(this, "Info", "Klasyfikacja zakończona w " + QString::number(counter) + " krokach, usunięto " + QString::number(cut_counter) + " punktów.");
        generateOutput();
    }
}

void MainWindow::generateOutput(){
     std::ofstream output;
     std::string filename = current_filename.toStdString().append("_output.arff");
     output.open(filename);
     output << "@relation output" << std::endl;//current_filename.toStdString() << std::endl;
     for(int i = 0; i < binary_vectors.at(0)->size(); ++i)
         output << "@attribute v" + QString::number(i).toStdString() << " {'0','1'}" << std::endl;
     output << "@attribute class {";
     for(int i = 0; i < known_classes.size();++i){
         output << "'" << known_classes.at(i) << "'";
         if( i < known_classes.size() - 1 )
            output << ",";
     }
     output << "}\n@data" << std::endl;
     for(int i = 0; i < classes.size(); ++i){
         for(int j = 0; j < binary_vectors.at(i)->size(); ++j)
             output << binary_vectors.at(i)->at(j) << " ";

         output << classes.at(i) << "\n";
     }
     output.close();
}

void MainWindow::sl_openFile(){

    current_filename = QFileDialog::getOpenFileName( this, "Open data", "/home/maciek/Downloads/edzbiory");

    if(current_filename == NULL)
        return;

    QFile input_file(current_filename);

    if( input_file.open( QFile::ReadOnly | QFile::Text ) )
    {
        clearData();
        parseFile(input_file);
    }

}

void MainWindow::parseFile(QFile &input_file)
{
    QString input_line;
    bool first_iteration = true;
    double x_min, x_max, y_min, y_max;
    QVector<QVector<double>*> x_col, y_col;

    while( !input_file.atEnd() )
    {
        input_line = input_file.readLine();
        if(input_line == "\n")
            continue;

        QStringList l = input_line.split(QRegExp("(\\ |\\,|\\;|\\t)"));

        if(first_iteration){
            for(int i = 0; i < l.size() - 1; ++i)
                data.push_back(new std::vector<double>);

            x_min = l.at(0).toDouble();
            x_max = l.at(0).toDouble();
            y_min = l.at(1).toDouble();
            y_max = l.at(1).toDouble();

            first_iteration = false;
        }

        for(int i = 0; i < l.size() - 1; ++i)
            data.at(i)->push_back(l.at(i).toDouble());

        if(data.at(0)->back() < x_min) x_min = data.at(0)->back();
        if(data.at(0)->back() > x_max) x_max = data.at(0)->back();
        if(data.at(1)->back() < y_min) y_min = data.at(1)->back();
        if(data.at(1)->back() > y_max) y_max = data.at(1)->back();

        classes.push_back(l.last().toInt());

        //wydzielanie zbiorow do rysowania
        //jesli klasy nie ma jeszcze w zbiorze znanych klas:
        int n = findElement(known_classes, classes.back() );
        if( n == -1) {
            known_classes.push_back(classes.back());
            x_col.push_back(new QVector<double>);
            y_col.push_back(new QVector<double>);
            x_col.last()->push_back(data.at(0)->back());
            y_col.last()->push_back(data.at(1)->back());
        }
        else {
            x_col.at(n)->push_back(data.at(0)->back());
            y_col.at(n)->push_back(data.at(1)->back());
        }
    }

    //new nie usuniete
    plotData(x_col, y_col, x_min, x_max, y_min, y_max);

    for(int i = 0; i < data.at(0)->size(); ++i) {
        erased.push_back(false);
        binary_vectors.push_back(new std::vector<int>);
    }
}
void MainWindow::plotCutPoints(const std::vector<double> &xcol, const std::vector<double> &ycol, const std::vector<int> &indexes){
    if(indexes.empty())
        return;

    QVector<double> x,y;
    for(auto a: indexes){
        x.push_back(xcol.at(a));
        y.push_back(ycol.at(a));
    }
    plotw->addGraph();
    int s = plotw->graphCount() - 1;
    plotw->graph(s)->setData(x, y);
    plotw->graph(s)->setLineStyle(QCPGraph::lsNone);
    plotw->graph(s)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, 2));
    plotw->graph(s)->setPen(QPen(Qt::black));
    plotw->replot();
}

void MainWindow::plotData(QVector<QVector<double>*> &xcol, QVector<QVector<double>*> &ycol,
                          double x_min, double x_max, double y_min, double y_max)
{
    const double d = 0.25f;

    if(plotw->graphCount() > 0){
        for(int i= 0; i < plotw->graphCount(); ++i)
            plotw->removeGraph(i);
        plotw->clearGraphs();
        plotw->clearItems();
    }
    for(signed i = 0; i < xcol.size(); ++i){
        plotw->addGraph();
        plotw->graph(i)->setData(*xcol.at(i), *ycol.at(i));
        plotw->graph(i)->setLineStyle(QCPGraph::lsNone);
        plotw->graph(i)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 4));
        plotw->graph(i)->setPen(QPen( QColor::fromHsv((70*i+30)%255,255,190) ) );
    }

    plotw->xAxis->setRange(x_min - d, x_max + d);
    plotw->yAxis->setRange(y_min - d, y_max + d);
    plotw->replot();
}
void MainWindow::plotLine(int axis, double l){
    QCPItemLine *line = new QCPItemLine(plotw);
    plotw->addItem(line);
    if(axis == 0){//x axis
        line->start->setCoords(l, -99);
        line->end->setCoords(l, 99);
        line->setPen(QPen(QColor::fromRgb(255,0,0)));
    }
    else{//y axis
        line->start->setCoords(-99, l);
        line->end->setCoords(99,l);line->setPen(QPen(QColor::fromRgb(0,0,255)));
    }
    plotw->replot();

}
void MainWindow::clearData(){
    for(unsigned i = 0; i < data.size(); ++i)
        delete data.at(i);
    for(unsigned i = 0; i < binary_vectors.size(); ++i)
        delete binary_vectors.at(i);
    step_results.clear();
    step_results_dir.clear();
    data.clear();
    binary_vectors.clear();
    classes.clear();
    known_classes.clear();
    erased.clear();
    counter = 0;
    cut_counter = 0;
}
