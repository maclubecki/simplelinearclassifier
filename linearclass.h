#ifndef LINEARCLASS
#define LINEARCLASS
#include <QVector>
#include <QStringList>
#include <vector>
#include <thread>
#include <iostream>
#include <ctime>

struct conflict_st{
    int attribute;
    int class_id;
    int line_dir;
    int adjustment = 0;
    double line;
};

int countAdjustment(const std::vector<std::pair<int,int>> &v, int &max_index, int &sum);
std::pair<double,double> vectMinMax(const std::vector<double> &v, const std::vector<bool> &erased);
std::pair<int,int> maxGroup(const std::vector< std::pair<int,int> > &v, int &index);
bool verifyInput(const QString &s, const int cols, std::vector<double> &output);
bool vectEquality(const std::vector<int> &a, const std::vector<int> &b);
bool checkGroups(const std::vector<std::pair<int,int> > &groups);
int findElement(const std::vector<int> &v, int e);
int fIndex(const std::vector<std::pair<int,int> > &v, int cl);
conflict_st findBestCut(const std::vector<conflict_st> &conflicts, int &index);
conflict_st cheapestCut(const std::vector<double> &v, const std::vector<int> &c, const std::vector<bool> &erased, std::vector<std::pair<int,int>> &groups_l,
                        std::vector<std::pair<int,int>> &groups_r, const double line, const int last_i);
void cutPoints(const std::vector<double> &v, const std::vector<int> &c, std::vector<bool> &erased,
               const int cut_class, const double line, const int line_dir, std::vector<int> &cut_indexes, int &cut_count);
bool checkEnd(const std::vector<int> &classes, const std::vector<bool> &erased);
void checkLine(const std::vector<double> &v, const std::vector<int> &c, const std::vector<int> &class_list,
               const std::vector<bool> &erased, const double line, std::pair<int,int> &result, int &side, conflict_st &conflict);
bool splitStep(std::vector<std::vector<double>*> &v, const std::vector<int> &c, const std::vector<int> &known_classes,
               std::vector<bool> &erased, std::vector<std::vector<int>*> &binary_vectors, std::vector<int> &dir_results,
               std::vector<int> &cut_points, int &cut_count, std::pair<int,double> &final_result);
std::pair<int, double> splitStep(std::vector<std::vector<double>*> &v, const std::vector<int> &c, const std::vector<int> &known_classes,
                                 std::vector<bool> &erased, std::vector<std::vector<int> *> &binary_vectors, std::vector<int> &dir_results, std::vector<int> &cut_points,
                                 int &cut_count);
void finalize(const std::vector<double> &v, const std::vector<int> &c, const int result_class, const int side, std::vector<bool> &erased, const double line, std::vector<std::vector<int> *> &binary_vectors);
int classifyPoint(const std::vector<std::pair<int,double>> &steps, const std::vector<int> &steps_dir,
                  const std::vector<std::vector<int>*> &binary_vectors, const std::vector<int> &classes, const std::vector<double> &newpoint);


#endif // LINEARCLASS
